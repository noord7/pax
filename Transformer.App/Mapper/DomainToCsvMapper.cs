﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transformer.App.Domain.External;
using Transformer.App.Domain.Internal;
using Transformer.Core.Mapper;

namespace Transformer.App.Mapper
{
    public class DomainToCsvMapper : IMapper<List<InAppAddress>, List<CsvModel>>
    {
        public List<CsvModel> Map(List<InAppAddress> source)
        {
            List<CsvModel> models = new List<CsvModel>();

            foreach (var item in source)
            {
                models.Add(new CsvModel()
                {
                    CityCode = item.CityCode,
                    CityName = item.CityName,
                    DistrictName = item.DistrictName,
                    ZipCode = item.ZipCode
                });
            }
            return models;
        }
    }
}
