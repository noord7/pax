﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transformer.App.Domain.External;
using Transformer.App.Domain.Internal;
using Transformer.Core.Mapper;

namespace Transformer.App.Mapper
{
    public class XmlToDomainMapper : IMapper<AddressInfo, List<InAppAddress>>
    {
        public List<InAppAddress> Map(AddressInfo source)
        {
            List<InAppAddress> addr = new List<InAppAddress>();

            foreach (var city in source.City)
            {
                foreach (var district in city.District)
                {
                    foreach (var zip in district.Zip)
                    {
                        addr.Add(new InAppAddress()
                        {
                            CityCode = city.code,
                            CityName = city.name,
                            DistrictName = district.name,
                            ZipCode = zip.code
                        });
                    }
                }
            }
            return addr;
        }
    }
}
