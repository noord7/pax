﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transformer.App.Domain.External;
using Transformer.App.Domain.Internal;
using Transformer.Core.Mapper;

namespace Transformer.App.Mapper
{
    // HATE XML
    public class DomainToXmlMapper : IMapper<List<InAppAddress>, AddressInfo>
    {
        public AddressInfo Map(List<InAppAddress> source)
        {
            List<AddressInfoCity> cities = new List<AddressInfoCity>();

            var grouped = source.GroupBy(x => new { x.CityName, x.CityCode }).ToDictionary(o => o.Key, o => o.ToList());

            foreach (var item in grouped)
            {
                var city = new AddressInfoCity()
                {
                    name = item.Key.CityName,
                    code = item.Key.CityCode
                };

                var districtGroup = item.Value.GroupBy(x => x.DistrictName).ToDictionary(o => o.Key, o => o.ToList());

                List<AddressInfoCityDistrict> districts = new List<AddressInfoCityDistrict>();
                foreach (var district in districtGroup)
                {
                    
                    var d = new AddressInfoCityDistrict()
                    {
                        name = district.Key
                    };

                    List<AddressInfoCityDistrictZip> zipcodes = new List<AddressInfoCityDistrictZip>();
                    foreach (var zipcode in district.Value)
                        zipcodes.Add(new AddressInfoCityDistrictZip() { code = zipcode.ZipCode });

                    d.Zip = zipcodes.ToArray();
                    districts.Add(d);
                }
                city.District = districts.ToArray();
                cities.Add(city);
            }

            return new AddressInfo() { City = cities.ToArray() };
        }
    }
}
