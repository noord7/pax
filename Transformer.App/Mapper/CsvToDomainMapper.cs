﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transformer.App.Domain.External;
using Transformer.App.Domain.Internal;
using Transformer.Core.Mapper;

namespace Transformer.App.Mapper
{
    public class CsvToDomainMapper : IMapper<List<CsvModel>, List<InAppAddress>>
    {
        public List<InAppAddress> Map(List<CsvModel> source)
        {
            List<InAppAddress> addr = new List<InAppAddress>();

            foreach (var csv in source)
            {
                addr.Add(Map(csv));
            }

            return addr;
        }

        private InAppAddress Map(CsvModel model)
        {
            return new InAppAddress()
            {
                CityCode = model.CityCode,
                CityName = model.CityName,
                DistrictName = model.DistrictName,
                ZipCode = model.ZipCode
            };
        }
    }
}
