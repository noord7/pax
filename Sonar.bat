SonarQube.Scanner.MSBuild.exe begin /k:"hwa" /n:"hwa" /v:"0.1" /d:sonar.cs.opencover.it.reportsPaths="%CD%\coverage.xml"
MSBuild

packages\OpenCover.4.6.519\tools\OpenCover.Console.exe -target:"vstest.console.exe"  -targetargs:"Transformer.UnitTest\bin\Debug\Transformer.UnitTest.dll /UseVsixExtensions:true /InIsolation /Logger:trx" -mergebyhash -skipautoprops -output:"coverage.xml" -register:user

SonarQube.Scanner.MSBuild.exe end