﻿using CsvHelper;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Transformer.Core.Deserializer;
using System;

namespace Transformer.App.Deserializers
{
    public class CsvDeserializer<T> : IDeserializer<List<T>>
    {
        public List<T> Deserialize(string input)
        {
            using (TextReader tr = new StringReader(input))
            {
                var csv = new CsvReader(tr);
                var records = csv.GetRecords<T>().ToList();
                records.RemoveAt(0);
                return records;
            }
        }
    }
}
