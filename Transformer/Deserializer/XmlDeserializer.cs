﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using Transformer.Core.Deserializer;

namespace Transformer.App.Deserializers
{
    public class XmlDeserializer<T> : IDeserializer<T>
    {
        public T Deserialize(string input)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(input));
            T resultingMessage = (T)serializer.Deserialize(memStream);

            return resultingMessage;
        }
    }
}
