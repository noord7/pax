﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformer.Core.Deserializer
{
    public interface IDeserializer<out T>
    {
        T Deserialize(string input);
    }
}
