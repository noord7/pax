﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformer.Core.Mapper
{
    public interface IMapper<in Source, out Target>
    {
        Target Map(Source source);
    }
}
