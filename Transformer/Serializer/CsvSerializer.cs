﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transformer.Core.Deserializer;

namespace Transformer.Core.Serializer
{
    public class CsvSerializer<T> : ISerializer<List<T>>
    {
        public string Serialize(List<T> data)
        {
            var properties = typeof(T).GetProperties();
            var result = new StringBuilder();

            foreach (var row in data)
            {
                var values = properties.Select(p => p.GetValue(row, null));
                var line = string.Join(",", values);
                result.AppendLine(line);
            }

            return result.ToString();
        }
    }
}