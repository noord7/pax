﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transformer.Core.Serializer
{
    public interface ISerializer<in T>
    {
        string Serialize(T Model);
    }
}
