﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Transformer.Core.Serializer
{
    public class XmlSerializer<T> : ISerializer<T>
    {
        public string Serialize(T Model)
        {
            var stringwriter = new System.IO.StringWriter();
            var serializer = new XmlSerializer(typeof(T));
            serializer.Serialize(stringwriter, Model);
            return stringwriter.ToString();
        }
    }
}
