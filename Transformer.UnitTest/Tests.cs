﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Transformer.App.Deserializers;
using Transformer.App.Domain.External;
using Transformer.App.Domain.Internal;
using Transformer.App.Mapper;
using Transformer.Core.Deserializer;
using Transformer.Core.Mapper;
using Transformer.Core.Serializer;

namespace Transformer.UnitTest
{
    [TestFixture]
    public class Tests
    {
        private string xmlpath;
        private string csvpath;

        private IDeserializer<AddressInfo> xmldeserializer;
        private IDeserializer<List<CsvModel>> csvDeserializer;

        private IMapper<List<CsvModel>, List<InAppAddress>> csvToDomainMapper;
        private IMapper<AddressInfo, List<InAppAddress>> xmlToDomainMapper;

        private IMapper<List<InAppAddress>, List<CsvModel>> domainToCsvMapper;
        private IMapper<List<InAppAddress>, AddressInfo> domainToXmlMapper;

        private ISerializer<AddressInfo> xmlSerializer;
        private ISerializer<List<CsvModel>> csvSerializer;

        private static readonly string basePath = AppDomain.CurrentDomain.BaseDirectory + "..\\..\\..\\";

        [SetUp]
        public void Init()
        {
            xmlpath = $"{basePath}sample_data.xml";
            csvpath = $"{basePath}sample_data.csv";

            csvToDomainMapper = new CsvToDomainMapper();
            xmlToDomainMapper = new XmlToDomainMapper();
            domainToXmlMapper = new DomainToXmlMapper();
            domainToCsvMapper = new DomainToCsvMapper();

            xmldeserializer = new XmlDeserializer<AddressInfo>();
            csvDeserializer = new CsvDeserializer<CsvModel>();

            xmlSerializer = new XmlSerializer<AddressInfo>();
            csvSerializer = new CsvSerializer<CsvModel>();
        }

        [Test]
        public void TestCase1()
        {
            var content = File.ReadAllText(csvpath);
            var mapped = csvDeserializer.Deserialize(content);
            var results = csvToDomainMapper.Map(mapped);
            var t = results.Where(x => x.CityName == "Antalya").ToList();
            var formed = domainToXmlMapper.Map(t);
            var res = xmlSerializer.Serialize(formed);
            File.WriteAllText(basePath + "TestCase1.txt", res);
            Console.WriteLine(res);
        }

        [Test]
        public void TestCase2()
        {
            var content = File.ReadAllText(csvpath);
            var mapped = csvDeserializer.Deserialize(content);
            var results = csvToDomainMapper.Map(mapped);
            var t = results.OrderBy(x => x.CityName).ThenBy(x => x.DistrictName).ToList();
            var formed = domainToCsvMapper.Map(t);
            var res = csvSerializer.Serialize(formed);
            File.WriteAllText(basePath + "TestCase2.txt", res);
            Console.WriteLine(res);
        }

        [Test]
        public void TestCase3()
        {
            var content = File.ReadAllText(xmlpath);
            var mapped = xmldeserializer.Deserialize(content);
            var results = xmlToDomainMapper.Map(mapped);
            var t = results.Where(x => x.CityName == "Ankara").OrderByDescending(x => x.ZipCode).ToList();
            var formed = domainToCsvMapper.Map(t);
            var res = csvSerializer.Serialize(formed);
            File.WriteAllText(basePath + "TestCase3.txt", res);
            Console.WriteLine(res);
        }
    }
}
